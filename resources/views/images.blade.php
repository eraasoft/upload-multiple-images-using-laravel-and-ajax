@foreach($images as $image)
    <div class="col-sm-4 p-3">
        <div class="card border" style="">
            <img src="{{asset('uploads/'.$image->image_name)}}" class="card-img-top border" height="350" >
        </div>
    </div>
@endforeach