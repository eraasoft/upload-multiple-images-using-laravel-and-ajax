<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>Hello, world!</title>
  </head>
  <body>

    <div class="container">
        <div class="row">
            <div class="col-sm-12 m-5 p-5">
                <h1 class="text-center text-white bg-success p-3">Upload Multi Image Using Laravel  & AJAX</h1>
                <hr>

                @if (session()->has('success'))
 
                    <div class="alert alert-success alert-block">
    
                        <button type="button" class="close" data-dismiss="alert">×</button>
    
                        <strong>{{ session()->get('success')}}</strong>
    
                    </div>
                @endif
    
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <form   enctype="multipart/form-data" id="uploadImages" >
                    <div id="errors"></div>
                    <div class="form-group">
                        <label> Select Images</label>
                        <input type="file" class="form-control" name="images[]" multiple  >
                    </div>
                    <div class="form-group">
                        <input type="submit" class=" btn btn-success"  value="Save"  >
                    </div>

                    @csrf
                </form>

                <hr>

                

            </div>
        </div>

        <div class="row show-images">
            
            @foreach($images as $image)
            <div class="col-sm-4 p-3">
                <div class="card border" style="">
                    <img src="{{asset('uploads/'.$image->image_name)}}" class="card-img-top border" height="350" >
                </div>
            </div>
            @endforeach
        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" ></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" ></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" ></script>


    <script>
    
    
        $("#uploadImages").submit(function(e){

            var formData  = new FormData(jQuery('#uploadImages')[0]);
            e.preventDefault();
            $.ajax({
                url:"{{url('/store-images')}}",
                type:'POST',
                data:formData,
                contentType:false,
                processData:false,
                success:function(data)
                {
                    $("#errors").html("<p class='alert alert-success'> Uploaded Success</p>");
                    $(".show-images").html(data)
                    
                },
                error:function(xhr, status, error)
                {
                    $("#errors").html('');
                    $.each(xhr.responseJSON.errors,function(key,item)
                    {
                        $("#errors").append("<p class='alert alert-danger'> "+ item +" </p>")
                    })
                }
            })


        });
    
    
    </script>

 
  </body>
</html>