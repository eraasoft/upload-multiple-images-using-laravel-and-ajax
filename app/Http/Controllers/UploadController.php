<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PostImage;

class UploadController extends Controller
{

    public function upload()
    {
        $images = PostImage::orderBy('id','DESC')->get();
        return view('uploadfiles',compact('images'));
    }


    public function uploadImages(Request $request)
    {

        $request->validate([
            'images' =>'required',
            'images.*' =>'image|mimes:png,jpg,gif,jpeg'
        ]);

        if($request->hasFile('images'))
        {
            foreach($request->images as $image)
            {
                $imageName = $image->getClientOriginalName();
                $imageExte = $image->getClientOriginalExtension();

                $newName = uniqid("",true).".".$imageExte;
                $image->move("uploads",$newName);

                $postImage = new PostImage;
                $postImage->image_name = $newName;
                $postImage->save();
            }
        }

        return back()->with("success","Uploaded Success");
    }






    public function showForm()
    {
        $images = PostImage::orderBy('id','DESC')->get();
        return view('uploadfilesWithAjax',compact('images'));
    }

    public function uploadImagesWithAjax(Request $request)
    {

        if($request->ajax())
        {
            $request->validate([
                'images' =>'required',
                'images.*' =>'image|mimes:png,jpg,gif,jpeg'
            ]);
    
            if($request->hasFile('images'))
            {
              
                foreach($request->images as $image)
                {
                    $imageName = $image->getClientOriginalName();
                    $imageExte = $image->getClientOriginalExtension();
    
                    $newName = uniqid("",true).".".$imageExte;
                    $image->move("uploads",$newName);
    
                    $postImage = new PostImage;
                    $postImage->image_name = $newName;
                    $postImage->save();
                }
            }
            
            $images = PostImage::orderBy('id','DESC')->get();
            return view('images',compact('images'));
          
        }
        
    }




}
